// 将二进制数据转换成16进制字符串
function byteArrayToHexString(bytes) {
    var val = "";
    for (var i = 0; i < bytes.length; i++) {
        var tmp = bytes[i];
        if (tmp < 0) {
            tmp = 256 + tmp;
        }
        tmp = tmp.toString(16);
        if ((tmp + "").length == 1) {
            tmp = "0" + tmp;
        }
        val += tmp;
    }
    return val;
}

function md5(string) {
    var md = java.security.MessageDigest.getInstance("MD5");// 生成一个MD5加密计算摘要
    md.update(java.lang.String(string).getBytes("UTF-8"));// 计算md5函数，字符编码是 UTF-8

    return byteArrayToHexString(md.digest());
}

// 调用示例
// console.log( md5('123') );

let sto = storages.create('enter_permit_storge');

// sto.put("a","23123123aaaaa")

let a = sto.get("a")
log(a)