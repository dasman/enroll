let Base64A = android.util.Base64;

let message = "1231321315";
log("明文: ", message);

// 密钥，由于AES等算法要求是16位的倍数，我们这里用一个16位的密钥
let key = new $crypto.Key("password12345678");
log("密钥: ", key);

// AES加密
let aes = $crypto.encrypt(message, key, "AES/ECB/PKCS5padding");

// let eaes = Base64A.encodeToString(aes,Base64A.DEFAULT);

let daes = Base64A.Base64A.decode(aes,Base64A.DEFAULT);
let dsaes = new java.lang.String(daes).toString();

log(dsaes)

// log("AES加密后二进制数据: ", Base64A.encodeToString(aes,Base64A.DEFAULT));
// log("AES解密: ", $crypto.decrypt(aes, key, "AES/ECB/PKCS5padding", {output: 'string'}));