ui;
ui.layout(
    <drawer id="drawer">
        <vertical>
            <appbar>
                <toolbar id="toolbar" />
                <tabs id="tabs" />
            </appbar>
            <vertical>
                <viewpager id="viewpager" layout_weight="1">
                    <frame>
                        <list id="abnormal_friend_list">
                            <horizontal padding="0 8">
                                <checkbox id="selected_checkbox" layout_gravity="center" checked="{{selected}}" />
                                <vertical>
                                    <text id="friend_remark_text" text="{{friend_remark}}" maxLines="1" ellipsize="end" />
                                    <text id="we_chat_id_text" text="{{we_chat_id}}" maxLines="1" ellipsize="end" />
                                    <text id="abnormal_message_text" text="{{abnormal_message}}" maxLines="1" ellipsize="end" />
                                </vertical>
                            </horizontal>
                        </list>
                    </frame>
                    <frame>
                        <list id="normal_friend_list">
                            <horizontal padding="0 8">
                                <checkbox id="selected_checkbox" layout_gravity="center" checked="{{selected}}" />
                                <vertical>
                                    <text id="friend_remark_text" text="{{friend_remark}}" maxLines="1" ellipsize="end" />
                                    <text id="we_chat_id_text" text="{{we_chat_id}}" maxLines="1" ellipsize="end" />
                                </vertical>
                            </horizontal>
                        </list>
                    </frame>
                    <frame>
                        <list id="ignored_friend_list">
                            <text padding="8" text="{{friend_remark}}" maxLines="1" ellipsize="end" />
                        </list>
                    </frame>
                </viewpager>
                <horizontal bg="#EBEBEB">
                    <button id="previous_page_button" layout_weight="1" style="Widget.AppCompat.Button.Borderless" textStyle="bold" />
                    <text id="current_page_text" textStyle="bold" />
                    <text textStyle="bold" text="  /  " />
                    <text id="total_page_text" textStyle="bold" />
                    <button id="next_page_button" layout_weight="1" style="Widget.AppCompat.Button.Borderless" textStyle="bold" />
                </horizontal>
                <horizontal bg="#EBEBEB">
                    <button id="clear_friends_button" layout_weight="1" style="Widget.AppCompat.Button.Borderless" textStyle="bold" />
                    <button id="delete_friends_button" layout_weight="1" textColor="#CC0000" style="Widget.AppCompat.Button.Borderless" textStyle="bold" />
                    <button id="test_friends_button" layout_weight="1" textColor="#008274" style="Widget.AppCompat.Button.Borderless" textStyle="bold" />
                </horizontal>
            </vertical>
        </vertical>
        <vertical layout_gravity="left" bg="#FFFFFF">
            <list id="side_list">
                <horizontal>
                    <img w="50" h="50" padding="8" src="11" tint="#009688" />
                    <text textColor="black" textSize="16sp" text="1" layout_gravity="center" />
                </horizontal>
            </list>
        </vertical>
    </drawer>
);
