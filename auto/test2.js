// 加密
let str = java.lang.String("halou");
let key = new java.lang.String("my_key2342344444");

let cipher = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
let secretKeySpec = new javax.crypto.spec.SecretKeySpec(key.getBytes(), "AES");

let ivParameterSpec = new javax.crypto.spec.IvParameterSpec(key.getBytes());
cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
let encrypted = cipher.doFinal(str.getBytes("utf-8"));

// 解密
cipher.init(javax.crypto.Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
let decrypted = cipher.doFinal(encrypted);

toastLog(encrypted);
toastLog(decrypted);
