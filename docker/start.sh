appjar=""
if [ "$1" = "" ]
then
	echo "appjar is not set"
else
	echo "appjar is set $1"
	appjar=$1
fi

configfile=""
if [ "$2" = "" ]
then
	echo "configfile is not set"
else
	echo "configfile is set $2"
	configfile=$2
fi

jvm=""
if [ "$3" = "" ]
then
	echo "jvm is not set"
else
	echo "jvm is set $3"
	jvm=$3
fi

apm=""
if [ "$4" = "" ]
then
	echo "apm is off"
else
	echo "apm is on"
	apm="-Dskywalking.agent.service_name=$4 -javaagent:/skywalking/agent/skywalking-agent.jar"
fi

java $apm $jvm -jar $appjar --spring.config.location=$configfile
