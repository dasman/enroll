<!DOCTYPE html>
<html>
<head lang="en">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="email=no"/>
    <meta charset="UTF-8">
    <script type="text/javascript" src="${(domainUrlUtil.staticResources)!}/js/common.js"></script>
    <script src="${(domainUrlUtil.staticResources)!}/js/jquery.js"></script>
    <link href="${(domainUrlUtil.staticResources)!}/style/reset.css" rel="stylesheet" type="text/css">
    <link href="${(domainUrlUtil.staticResources)!}/style/base.css" rel="stylesheet" type="text/css">
    <link href="${(domainUrlUtil.staticResources)!}/style/header.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="${(domainUrlUtil.staticResources)!}/style/projectMessage.css"/>
    <link rel="stylesheet" href="${(domainUrlUtil.staticResources)!}/style/alert.css"/>

    <title>登记</title>
</head>
<body>
<header>&nbsp;&nbsp;&nbsp;&nbsp;登记<b>帮助</b></header>
<nav>
    <ul class="df jcsb">
        <li>
            <div class="activeTop">1</div>
            <p class="tc f26 activeBottom">登记</p>
        </li>
        <li>
            <div  >2</div>
            <p class="tc f26 ">成为vip</p>
        </li>
        <li>
            <div>3</div>
            <p class="tc f26">自动续办</p>
        </li>

</nav>
<form id="enrollfirst">
    <ul class="bgwh">
        <li class="df pr30 pl30">
            <label class="f26" for="name">北警账号</label>
            <input type="text" id="name" name="name" placeholder="请填写北京交警账号"/>
        </li>
        <li class="df pr30 pl30">
            <label class="f26" for="name">北警密码</label>
            <input type="text" id="password" name="password" placeholder="请填写北京交警密码"/>
        </li>
        <li class="df pr30 pl30">
            <label class="f26" for="isEnter">曾经办过</label>
            <select name="isEnter" id="isEnter" class="background">
                <option value="1"><p>是</p>已办理过进京证</option>
                <option value="0"><p>否</p>未办理过进京证</option>
            </select>
        </li>
        <li class="df pr30 pl30">
            <label class="f26" for="phone">手机号</label>
            <input type="text" id="phone" name="phone" placeholder="请填写手机号"/>
        </li>
        <li class="df pr30 pl30" id="reCodeLi">
            <label class="f26" for="reCode">推荐码</label>
            <input type="text" id="reCode" name="reCode" placeholder="请填写推荐码"/>
        </li>
        <li class="df pr30 pl30">
            <input type="text" id="verifyCode" name="verifyCode" placeholder="请输入图片验证码"/>
            <img onclick="refreshCode();" id="code_img" src="${(domainUrlUtil.urlResources)!}/system/verifyCode" width="32%" height="82%">

        </li>

        <li class="df pr30 pl30" style="border-top: 0.2rem solid #f2f2f2;">
            <label class="f26" >进京证类型</label>
            <span class="g6">六环外</span>
        </li>
        <li class="df pr30 pl30">
            <label class="f26" >续办日期</label>
            <span class="g6">明天</span>
        </li>

    </ul>

</form>
<input id="firstButton" type="submit" value="下一步"/>

<footer>
    <ul class="df pr30 pl30 jcsa">
        <li>
            <p  class="active">登记</p>
        </li>
        <li>
            <p >我的</p>
        </li>
    </ul>
</footer>
<script src="${(domainUrlUtil.staticResources)!}/js/alert.js"></script>

<script type="text/javascript">

$(function(){
		$("#firstButton").click(function() {
			var name = $("#name").val();
			if (name == '') {
				jqueryAlert({'content':'用户名不能为空','closeTime':2000,})
				return;
			}
			if (name.length < 6) {
				jqueryAlert({'content':'用户名至少6位','closeTime':2000,})
				return;
			}
			var password = $("#password").val();
			if (password == '') {
				jqueryAlert({'content':'密码不能为空','closeTime':2000,})
				return;
			}
			if (password.length < 6) {
				jqueryAlert({'content':'密码至少6位','closeTime':2000,})
				return;
			}
			var phone = $("#phone").val();

			var verifyCode = $("#verifyCode").val();
			if (verifyCode == '') {
				jqueryAlert({'content':'请输入验证码','closeTime':2000,})
				return;
			}
			if (verifyCode.length != 4) {
				jqueryAlert({'content':'请输入4位验证码','closeTime':2000,})
				return;
			}
			if ($("#enrollfirst").valid()) {
				$(".ahover").attr("disabled", "disabled");
				var params = $('#enrollfirst').serialize();
				$.ajax({
					type : "POST",
					url : domain + "/enroll/add",
					dataType : "json",
					async : false,
					data : params,
					success : function(data) {
						if (data.success) {
							window.location = domain + "/member/index.html";
						} else {
							jqueryAlert({'content':data.message,'closeTime':2000,})
							//刷新验证码
							refreshCode();
							$(".ahover").removeAttr("disabled");
						}
					},
					error : function() {
						jqueryAlert({'content' : '异常，请重试！','closeTime' : 2000,})
						$(".ahover").removeAttr("disabled");
					}
				});
			}

		});

	});

	var $_GET = (function(){
        var url = window.document.location.href.toString(); //获取的完整url
        var u = url.split("?");
        if(typeof(u[1]) == "string"){
            u = u[1].split("&");
            var get = {};
            for(var i in u){
                var j = u[i].split("=");
                get[j[0]] = j[1];
            }
            return get;
        } else {
            return {};
        }
    })();
    // 如果有传推荐码，推荐码隐藏
    var reCode = $_GET['reCode'];
    if(reCode != '' && reCode != undefined){
		 $("#reCode").val(reCode);
		 $("#reCodeLi").hide();
    }
    //刷新验证码
    function refreshCode() {
		$("#code_img").attr(
				"src",
				"${(domainUrlUtil.urlResources)!}/system/verifyCode?d"
				+ new Date().getTime());
    }

</script>

</body>
</html>