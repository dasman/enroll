﻿<#include "/seller/commons/_detailheader.ftl" />

<div class="layui-fluid">
	<div class="layadmin-tips">
		<i class="layui-icon" face>&#xe664;</i>

		<div class="layui-text" style="font-size: 20px;">
			好像出错了呢
		</div>

	</div>
</div>

<script src="${(domainUrlUtil.staticResources)!}/resources/layui/layui.js"></script>
<script>
	layui.config({
		base: '${(domainUrlUtil.staticResources)!}/resources/'
	}).extend({
		index: 'lib/index' //主入口模块˙
	});

	layui.use(['index'], function(){
		var $ = layui.$
				,admin = layui.admin;

	});

</script>
<#include "/seller/commons/_detailfooter.ftl" />