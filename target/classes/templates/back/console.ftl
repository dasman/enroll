<#include "/back/commons/_detailheader.ftl" />
<style type="text/css">
    .layui-table td .layui-table-cell{
        height:80px;
        line-height:80px;
    }
</style>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">

        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    商品总数
                    <span class="layui-badge layui-bg-blue layuiadmin-badge">总数</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font">${(productCountAll)!'0'}</p>
                    <p>
                        待售商品
                        <span class="layuiadmin-span-color">${(productCount)!'0'} <i class="layui-inline layui-icon layui-icon-flag"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    总订单数
                    <span class="layui-badge layui-bg-cyan layuiadmin-badge">总数</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font">${(orderCount)!'0'}</p>
                    <p>
                        未付款的订单
                        <span class="layuiadmin-span-color">${(orderCount1)!'0'}<i class="layui-inline layui-icon layui-icon-face-smile-b"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    订单总金额
                    <span class="layui-badge layui-bg-green layuiadmin-badge">总数</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">${(orderAllMoney)!''}</p>
                    <p>
                        本月的订单金额
                        <span class="layuiadmin-span-color">${(orderMonthMoney)!''}<i class="layui-inline layui-icon layui-icon-dollar"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">
                    用户咨询
                    <span class="layui-badge layui-bg-orange layuiadmin-badge">总数</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">

                    <p class="layuiadmin-big-font">${(productCommentsCount)!'0'}</p>
                    <p>
                        未处理
                        <span class="layuiadmin-span-color">${(productCommentsCountNo)!'0'}<i class="layui-inline layui-icon layui-icon-user"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm12">
            <div class="layui-card">
                <div class="layui-card-header">
                    全国订单分布统计(统计已完成订单的总金额)
                </div>
                <div class="layui-card-body">
                    <div class="layui-row">
                        <div class="layui-col-sm8">
                            <div class="layui-card-body" style="padding:0 6px 6px 6px;">
                                <div id="yxkj-echarts1" style="width: 700px;height:320px;margin: 0 auto;"></div>
                            </div>
                        </div>
                        <div class="layui-col-sm4">
                            <div class="layuiadmin-card-list">
                                <p class="layuiadmin-normal-font">店铺服务评分（${(seller.scoreService)!'0'}分）</p>
                                <div id="rate-service"></div>
                            </div>
                            <div class="layuiadmin-card-list">
                                <p class="layuiadmin-normal-font">店铺发货评分（${(seller.scoreDeliverGoods)!'0'}分）</p>
                                <div id="rate-goods"></div>
                            </div>
                            <div class="layuiadmin-card-list">
                                <p class="layuiadmin-normal-font">店铺描述评分（${(seller.scoreDescription)!'0'}分）</p>
                                <div id="rate-description"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-col-sm8">
            <div class="layui-card">
                <div class="layui-card-header">最新订单</div>
                <div class="layui-card-body">
                    <table id="table-data"></table>
                    <script type="text/html" id="imgTpl">
                        {{#  layui.each(d.orderProductList, function(index, item){ }}
                        <img height="75" src=${(domainUrlUtil.imageResources)!}/{{ item.productLeadPicpath }}>
                        {{#  }); }}
                        {{#  if(d.orderProductList.length === 1){ }}
                        {{ d.orderProductList[0].productName }}
                        {{#  } }}
                    </script>
                </div>
            </div>
        </div>
        <div class="layui-col-sm4">
            <div class="layui-card">
                <div class="layui-card-header">系统公告</div>
                <div class="layui-card-body">
                    <ul class="layuiadmin-card-status layuiadmin-home2-usernote">


                                <li>
                                    <h3>标题</h3>
                                    <p>简介</p>
                                    <span>时间</span>
                                </li>

                    </ul>
                </div>
            </div>
        </div>


    </div>
</div>

<script src="${(domainUrlUtil.staticResources)!}/resources/layui/layui.js"></script>
<script src="${(domainUrlUtil.staticResources)!}/resources/modules/echarts.min.js"></script>
<script src="${(domainUrlUtil.staticResources)!}/resources/modules/china.js"></script>

<script>
    layui.config({
        base: '${(domainUrlUtil.staticResources)!}/back/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块˙
    }).use(['index', 'sample']);
    layui.use(['index', 'table', 'console', 'jquery', 'rate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,table = layui.table
            ,layer = layui.layer
            ,jquery = layui.jquery
            ,console = layui.console
            ,rate = layui.rate

        $(document).ready(function(){
            dataAll1('','','');
        });

        function dataAll1(startTime,endTime,sellerId) {
            $.ajax({
                type:"GET",
                url: "${domainUrlUtil.urlResources}/back/report/chinaorderJson",
                dataType: "json",
                data: "startTime="+startTime + "&endTime=" + endTime + "&sellerId=" + sellerId,
                cache:false,
                success:function(data){
                    if (data.success) {
                        dataJson = data.data;
                        var myChart = echarts.init(document.getElementById('yxkj-echarts1'));
                        option = {
                            title: {
                                left: 'center'
                            },
                            tooltip : {
                                trigger: 'item'
                            },
                            dataRange: {
                                orient: 'horizontal',
                                min: 0,
                                max: 55000,
                                color: ['#4d779c', '#c3e5dc'],
                                text:['高','低'],           // 文本，默认为数值文本
                                splitNumber:0
                            },
                            series : [
                                {
                                    name: '全国订单分布统计',
                                    type: 'map',
                                    mapType: 'china',
                                    mapLocation: {
                                        x: 'center'
                                    },
                                    selectedMode : 'multiple',
                                    itemStyle:{
                                        normal:{label:{show:true}},
                                        emphasis:{label:{show:true}}
                                    },
                                    data:dataJson
                                }
                            ]
                        };
                        myChart.setOption(option,true);
                    }
                }
            });
        }

        rate.render({
            elem: '#rate-service'
            ,value: ${(seller.scoreService)!0}
            ,readonly: true
        });
        rate.render({
            elem: '#rate-goods'
            ,value: ${(seller.scoreDeliverGoods)!0}
            ,readonly: true
        });
        rate.render({
            elem: '#rate-description'
            ,value: ${(seller.scoreDescription)!0}
            ,readonly: true
        });

        table.render({
            elem: '#table-data'
            ,url: '${domainUrlUtil.urlResources}/seller/order/orders/list'
            ,page: true
            // ,height: 665
            ,limit: 10
            ,cols: [[
                {field:'orderSn', title: '商品', width:300, templet: '#imgTpl'}
                ,{field:'orderSn', title: '子订单号', width:200}
                ,{field:'orderPsn', title: '父订单号', width:200}
                ,{field:'memberName', title: '买家用户名', width:100}
                ,{field:'orderType', title: '订单类型', width:150, templet:'<div>{{orderType(d.orderType)}}</div>'}
                ,{field:'paymentStatus', title: '付款状态', width:100, templet:'<div>{{paymentStatus(d.paymentStatus)}}</div>'}
                ,{field:'orderState', title: '订单状态', width:100, templet:'<div>{{getState(d.orderState)}}</div>'}
                ,{field:'source', title: '订单来源', width:100, templet:'<div>{{orderSource(d.source)}}</div>'}
                ,{field:'moneyProduct', title: '商品金额', width:100}
                ,{field:'moneyOrder', title: '订单总金额', width:100}
                ,{field:'moneyLogistics', title: '物流金额', width:100}
                ,{field:'moneyCoupon', title: '优惠券金额', width:100}
                ,{field:'moneyActFull', title: '订单满减金额', width:120}
                ,{field:'moneyDiscount', title: '优惠总额（含单品立减）', width:200}
                ,{field:'moneyBack', title: '退款金额', width:100}
                ,{field:'moneyIntegral', title: '积分换算金额', width:120}
                ,{field:'integral', title: '使用积分', width:100}
                ,{field:'name', title: '收货人', width:100}
                ,{field:'mobile', title: '收货电话', width:120}
                ,{field:'addressAll', title: '省市区', width:150}
                ,{field:'addressInfo', title: '详细地址', width:200}
                ,{field:'paymentName', title: '支付方式', width:150}
                ,{field:'createTime', title: '创建时间', width:200}
            ]]
            ,parseData: function(res){ //将原始数据解析成 table 组件所规定的数据
                return {
                    "code": (res.success == true) ? 0 : -1, //解析接口状态
                    "msg": res.message, //解析提示文本
                    "count": res.total, //解析数据长度
                    "data": res.rows    //解析数据列表
                };
            }
        });

    });

    var codeBox;


    function getState(value) {
        var box = codeBox["ORDERS_ORDER_STATE"][value];
        return box;
    }

    function paymentStatus(value) {
        var box = codeBox["ORDER_PAYMENT_STATUS"][value];
        return box;
    }

    function invoiceStatus(value) {
        var box = codeBox["ORDER_INVOICE_STATUS"][value];
        return box;
    }

    function orderType(value) {
        var box = codeBox["ORDER_TYPE"][value];
        return box;
    }

    function orderEvaluate(value) {
        var box = codeBox["ORDERS_EVALUATE"][value];
        return box;
    }

    function orderSource(value) {
        var box = codeBox["MEMBER_SOURCE"][value];
        return box;
    }

</script>

<#include "/back/commons/_detailfooter.ftl" />