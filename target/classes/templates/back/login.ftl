<#include "/back/commons/_detailheader.ftl" />

<link rel="stylesheet" href="${(domainUrlUtil.staticResources)!}/back/style/login.css" media="all">

<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login">

	<div class="layadmin-user-login-main">
		<div class="layadmin-user-login-box layadmin-user-login-header">
			<h2>后台中心</h2>
			<p>后台中心</p>
		</div>
		<div class="layadmin-user-login-box layadmin-user-login-body layui-form">
			<div class="layui-form-item">
				<label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
				<input type="text" name="name" id="LAY-user-login-username" lay-verify="required" placeholder="用户名" class="layui-input">
			</div>
			<div class="layui-form-item">
				<label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
				<input type="password" name="password" id="LAY-user-login-password" lay-verify="required" placeholder="密码" class="layui-input">
			</div>
			<div class="layui-form-item">
				<div class="layui-row">
					<div class="layui-col-xs7">
						<label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
						<input type="text" name="verifyCode" id="LAY-user-login-vercode" lay-verify="required" placeholder="图形验证码" class="layui-input">
					</div>
					<div class="layui-col-xs5">
						<div style="margin-left: 10px;">
							<img onclick="refreshCode();" id="code_img" src='${(domainUrlUtil.urlResources)!}/system/verifyCode' width="100" height="34">
						</div>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<button class="layui-btn layui-btn-fluid" lay-submit lay-filter="submit-form">登   陆</button>
			</div>

		</div>
	</div>

	<div class="layui-trans layadmin-user-login-footer">

		<p>© 2020 <a href="http://www.sapuo.com/" target="_blank">sapuo</a></p>
		<p>
			<span><a href="http://www.sapuo.com/" target="_blank">获取授权</a></span>
			<span><a href="http://www.sapuo.com/" target="_blank">在线演示</a></span>
			<span><a href="http://www.sapuo.com/" target="_blank">前往官网</a></span>
		</p>
	</div>

</div>

<script src="${(domainUrlUtil.staticResources)!}/back/layui/layui.js"></script>
<script>
	var jquery;
	layui.config({
		base: '${(domainUrlUtil.staticResources)!}/back/' //静态资源所在路径
	}).extend({
		index: 'lib/index' //主入口模块
	}).use(['index', 'form', 'jquery'], function(){
		var $ = layui.$
				,admin = layui.admin
				,form = layui.form
				,layer = layui.layer;
		jquery = layui.jquery;

		form.render();

		form.on('submit(submit-form)', function(data){
			$.ajax({
				type:"POST",
				url: "${(domainUrlUtil.urlResources)!}/back/doLogin",
				dataType: "json",
				data: data.field,
				cache:false,
				success:function(data){
					if (data.code == 0) {
						window.location.href="${domainUrlUtil.urlResources}/back/index";
					} else {
						layer.msg(data.message);
						refreshCode();
					}
				}
			});
			return false;
		});

	});


	function refreshCode() {
		jquery("#code_img").attr(
				"src",
				"${(domainUrlUtil.urlResources)!}/system/verifyCode?d"
				+ new Date().getTime());
	}
</script>
<#include "/back/commons/_detailfooter.ftl" />