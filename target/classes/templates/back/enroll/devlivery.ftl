<#include "/seller/commons/_detailheader.ftl" />

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-body" style="padding: 15px;">
            <form class="layui-form" method="post" lay-filter="component-form-group">
                <input id="ccName" type="hidden" name="ccName" />
                <input id="ordersId" type="hidden" name="ordersId" value="${orders.id!}" />

                <div class="layui-form-item">
                    <label class="layui-form-label">子订单号：</label>
                    <div class="layui-form-mid yxkj-word-black">${(orders.orderSn)!''}</div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">物流公司：</label>
                    <div class="layui-input-block">
                        <select id="ccId" name="ccId" lay-filter="ccId" lay-verify="required">
                            <option value="">请选择</option>
                            <#list courierCompanylist as cc>
                                <option value="${cc.id!}" <#if orders.logisticsId==cc.id>selected='selected'</#if> >${cc.companyName!}</option>
                            </#list>
                        </select>
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label"><font style="color:red">*</font>快递单号</label>
                    <div class="layui-input-block">
                        <input type="text" id="giftNum" name="giftNum" value="${orders.logisticsNumber}" lay-verify="required" autocomplete="off" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item layui-hide">
                    <input type="button" lay-submit lay-filter="order-submit" id="order-submit" value="确认">
                </div>
            </form>
        </div>
    </div>
</div>


<script src="${(domainUrlUtil.staticResources)!}/resources/layui/layui.js"></script>
<script>
    layui.config({
        base: '${(domainUrlUtil.staticResources)!}/resources/'
    }).extend({
        index: 'lib/index' //主入口模块˙
    });

    layui.use(['index', 'form', 'jquery'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,jquery = layui.jquery
            ,form = layui.form;

        form.render(null, 'component-form-group');

        form.on('select(ccId)', function(dataObj){
            var ccId=document.getElementById("ccId");
            var index = ccId.selectedIndex; // 选中索引
            var ccName = ccId.options[index].text;//要的值
            $("#ccName").val(ccName);
            form.render(null, 'component-form-group');
        })


    });

</script>

<#include "/seller/commons/_detailfooter.ftl" />
