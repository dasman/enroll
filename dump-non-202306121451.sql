-- MySQL dump 10.13  Distrib 8.0.32, for macos13 (arm64)
--
-- Host: localhost    Database: non
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdkey`
--

DROP TABLE IF EXISTS `cdkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cdkey` (
  `id` bigint NOT NULL,
  `key` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `belong` varchar(20) DEFAULT NULL COMMENT '属于谁',
  `is_used` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdkey`
--

LOCK TABLES `cdkey` WRITE;
/*!40000 ALTER TABLE `cdkey` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdkey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enroll`
--

DROP TABLE IF EXISTS `enroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enroll` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `app_token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '北京交警token',
  `app_token_status` tinyint DEFAULT NULL COMMENT '北京交警token状态，0未录入1已录入，2失效',
  `apply_json` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '申请json数据',
  `enroll_status` tinyint DEFAULT NULL COMMENT '登记状态，0未支付，1已支付',
  `phone` varchar(20) DEFAULT NULL COMMENT '短信通知手机号',
  `re_code` varchar(100) DEFAULT NULL COMMENT '推荐码',
  `do_time` datetime DEFAULT NULL COMMENT '办理时间',
  `close_time` datetime DEFAULT NULL COMMENT '截止时间',
  `is_enter` tinyint DEFAULT NULL COMMENT '是否申请过进京证',
  `is_auto` tinyint DEFAULT NULL COMMENT '是否自动续办理',
  `create_time` datetime DEFAULT NULL COMMENT '登记时间',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enroll`
--

LOCK TABLES `enroll` WRITE;
/*!40000 ALTER TABLE `enroll` DISABLE KEYS */;
/*!40000 ALTER TABLE `enroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enroll_device`
--

DROP TABLE IF EXISTS `enroll_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enroll_device` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `fingerprint` varchar(100) DEFAULT NULL,
  `imei` varchar(100) DEFAULT NULL,
  `mac` varchar(100) DEFAULT NULL,
  `cdkey_id` bigint DEFAULT NULL,
  `cdkey` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enroll_device`
--

LOCK TABLES `enroll_device` WRITE;
/*!40000 ALTER TABLE `enroll_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `enroll_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enroll_log`
--

DROP TABLE IF EXISTS `enroll_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enroll_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `device_id` bigint DEFAULT NULL COMMENT '设备id',
  `status` tinyint DEFAULT NULL COMMENT '1开始办理，2办理失败，3办理成功',
  `msg_code` varchar(2000) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enroll_log`
--

LOCK TABLES `enroll_log` WRITE;
/*!40000 ALTER TABLE `enroll_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `enroll_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pay_log`
--

DROP TABLE IF EXISTS `pay_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pay_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `orders_id` int NOT NULL,
  `orders_sn` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `payment_code` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '支付方式code：INTEGRAL；BALANCE;PCUNIONPAY；H5UNIONPAY；PCALIPAY；H5ALIPAY；PCWXPAY；H5WXPAY',
  `payment_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '支付方式名称：积分支付；余额支付；PC银联；H5银联；PC支付宝；H5支付宝；PC微信；H5微信',
  `pay_money` decimal(10,2) NOT NULL COMMENT '支付金额(积分支付时为换算后的金额)',
  `pay_integral` int NOT NULL COMMENT '积分支付使用的积分数量',
  `pay_sn` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '支付订单号，第三方支付时传给第三方的订单号，积分和余额支付时不记录',
  `trade_sn` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '第三方支付交易流水号',
  `member_id` int NOT NULL,
  `member_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `create_time` datetime NOT NULL COMMENT '支付时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='订单支付日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pay_log`
--

LOCK TABLES `pay_log` WRITE;
/*!40000 ALTER TABLE `pay_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `pay_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_log`
--

DROP TABLE IF EXISTS `sale_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sale_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `opt_type` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '操作类型，C、新建，U、修改，D、删除',
  `seller_id` int NOT NULL COMMENT '商家ID',
  `sale_scale1` decimal(10,2) NOT NULL COMMENT '一级分销比例',
  `sale_scale2` decimal(10,2) NOT NULL COMMENT '二级分销比例',
  `state` tinyint NOT NULL DEFAULT '0' COMMENT '商家分销状态是否启用：0，未启用；1、启用',
  `create_id` int NOT NULL,
  `create_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_id` int NOT NULL,
  `update_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='分佣日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_log`
--

LOCK TABLES `sale_log` WRITE;
/*!40000 ALTER TABLE `sale_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_scale`
--

DROP TABLE IF EXISTS `sale_scale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sale_scale` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sale_scale` decimal(10,2) NOT NULL COMMENT '分销比例',
  `create_time` datetime NOT NULL,
  `type` tinyint DEFAULT NULL COMMENT '1一级，2二级',
  `code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='分佣用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_scale`
--

LOCK TABLES `sale_scale` WRITE;
/*!40000 ALTER TABLE `sale_scale` DISABLE KEYS */;
/*!40000 ALTER TABLE `sale_scale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'non'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-12 14:51:16
