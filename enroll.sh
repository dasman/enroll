#!/bin/bash

JAVA_PATH=/usr/bin/java
CONFIG_PATH=/enroll/var
LOG_PATH=/enroll/log
APP_PATH=/enroll/var
SPRING_PROFILE_LEVEL=prod
UPLOAD_APP_NAME=enroll-0.0.1-RELEASE.jar
APP_NAME=enroll.jar
JAVA_EXT_DIR=/usr/local/jdk/lib

MEM_XMS=512M
MEM_XMX=512M
MEM_XMN=256M
MEM_META_SPACE=512M
MEM_XSS=228k
GC_INTERVAL_TIME=3600000
NETWORKADDRESS_CACHE_TTL=600
SUN_NET_INETADDR_TTL=600

JAVA_OPTS="${JAVA_PATH} -server  -Xms${MEM_XMS} -Xmx${MEM_XMX} -Xmn${MEM_XMN} -XX:MetaspaceSize=${MEM_META_SPACE} -Xss${MEM_XSS}"

JAVA_OPTS="$JAVA_OPTS -Dsun.rmi.dgc.client.gcInterval=${GC_INTERVAL_TIME} -Dnetworkaddress.cache.ttl=${NETWORKADDRESS_CACHE_TTL} -Dsun.net.inetaddr.ttl=${SUN_NET_INETADDR_TTL} -Dfile.encoding=utf-8 "

JAVA_OPTS="$JAVA_OPTS -jar -Dspring.config.location=${CONFIG_PATH}/application.yml -Dlogging.file.path=${LOG_PATH} ${APP_NAME} --spring.profiles.active=${SPRING_PROFILE_LEVEL}  --logging.config=${CONFIG_PATH}/logback-spring.xml"

P_COUNT=`ps -ef|grep ${APP_NAME}|grep ${JAVA_PATH} |grep -v grep |wc -l`

P_NO=`ps -ef|grep ${APP_NAME}|grep ${JAVA_PATH}|grep -v grep |awk '{print $2}'`

CURRENT_TIME=`date +%Y%m%d%H%M%S`

start(){
        if [ $P_COUNT -ne 0 ]; then
                kill -9 $P_NO
        fi
        if [ ! -e "${APP_NAME}" ]; then
                echo "${APP_NAME} is not exists"
                exit 1
        fi

        echo ${JAVA_OPTS}
        ${JAVA_OPTS}  &
}

stop(){
        if [ $P_COUNT -ne 0 ]; then
                kill -9 $P_NO
        echo "app stop ok"
        fi
}

if [ "$1" = "" ]; then
 echo "please input param:start or stop"
elif [ "$1" = "start" ]; then
 start
elif [ "$1" = "stop" ]; then
 stop
else
 echo "param invalid ,please input param:start or stop"
fi