<#include "/back/commons/_detailheader.ftl" />

<div class="layui-fluid">
    <div class="layadmin-tips">
        <i class="layui-icon" face>&#xe664;</i>

        <div class="layui-text" style="font-size: 20px;">
            您无权访问该页面。
        </div>

    </div>
</div>

<script src="${(domainUrlUtil.staticResources)!}/back/layui/layui.js"></script>
<script>
	layui.config({
		base: '${(domainUrlUtil.staticResources)!}/back/'
	}).extend({
		index: 'lib/index' //主入口模块˙
	});

	layui.use(['index'], function(){
		var $ = layui.$
				,admin = layui.admin;

	});

</script>
<#include "/back/commons/_detailfooter.ftl" />