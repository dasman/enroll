<#include "/seller/commons/_detailheader.ftl" />
<style>
	.layui-upload-img{width: 150px; height: 150px; margin: 0 10px 10px 0;}
</style>
<div class="layui-fluid">
	<div class="layui-card">
		<div class="layui-card-body" style="padding: 15px;">
			<form class="layui-form" method="post" wid100 lay-filter="component-form-group">
				<fieldset class="layui-elem-field layui-field-title">
					<legend>订单基本信息</legend>
				</fieldset>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">子订单号：</label>
					<div class="layui-form-mid yxkj-word-black">${orders.orderSn!}</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">父订单号：</label>
					<div class="layui-form-mid yxkj-word-black">${orders.orderPsn!}</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">订单类型：</label>
					<div class="layui-form-mid yxkj-word-black"><@cont.codetext value="${(orders.orderType)!''}" codeDiv="ORDER_TYPE" /></div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">订单状态：</label>
					<div class="layui-form-mid yxkj-word-black"><@cont.codetext value="${(orders.orderState)!''}" codeDiv="ORDERS_ORDER_STATE" /></div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">下单时间：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.createTime?string('yyyy-MM-dd HH:mm:ss'))!''}</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">完成时间：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.finishTime?string('yyyy-MM-dd HH:mm:ss'))!''}</div>
				</div>


				<fieldset class="layui-elem-field layui-field-title">
					<legend>订单金额信息</legend>
				</fieldset>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">付款状态：</label>
					<div class="layui-form-mid yxkj-word-black"><@cont.codetext value="${(orders.ORDER_PAYMENT_STATUS)!''}" codeDiv="ORDER_TYPE" /></div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">支付方式：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.paymentName)!''}</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">订单总金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyOrder?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">商品金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyProduct?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">物流费用：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyLogistics?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">优惠总额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyDiscount?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">余额支付金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyPaidBalance?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">现金支付金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyPaidReality?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">优惠券金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyCoupon?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">订单满减金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyActFull?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">积分换算金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyIntegral?string('0.00'))!0.00 }</div>
				</div>

				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">退款的金额：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.moneyBack?string('0.00'))!0.00 }</div>
				</div>

				<fieldset class="layui-elem-field layui-field-title">
					<legend>订单发票信息</legend>
				</fieldset>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">发票状态：</label>
					<div class="layui-form-mid yxkj-word-black"><@cont.codetext value="${(orders.invoiceStatus)!''}" codeDiv="ORDER_INVOICE_STATUS" /></div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">发票抬头：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.invoiceTitle)!''}</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">纳税人识别号：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.taxNum)!''}</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">发票内容：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.invoiceType)!''}</div>
				</div>


				<fieldset class="layui-elem-field layui-field-title">
					<legend>订单收货人信息</legend>
				</fieldset>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">收货人姓名：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.name)!'' }</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">收货人邮编：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.zipCode)!'' }</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">收货人手机：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.mobile)!'' }</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">收货人邮箱：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.email)!'' }</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">收货人地址：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.addressAll)!''}${(orders.addressInfo)!''}</div>
				</div>


				<fieldset class="layui-elem-field layui-field-title">
					<legend>订单发货信息</legend>
				</fieldset>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">物流公司：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.logisticsName)!'' }</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">快递单号：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.logisticsNumber)!'' }</div>
				</div>
				<div class="layui-form-item yxkj-details-item">
					<label class="layui-form-label">发货时间：</label>
					<div class="layui-form-mid yxkj-word-black">${(orders.deliverTime?string('yyyy-MM-dd HH:mm:ss'))!'' }</div>
				</div>

				<fieldset class="layui-elem-field layui-field-title">
					<legend>网单基本信息</legend>
				</fieldset>

				<table class="layui-table">
					<colgroup>
						<col width="400">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
					<tr>
						<th>货品名称</th>
						<th>规格</th>
						<th>商品SKU</th>
						<th>商品单价</th>
						<th>商品数量</th>
						<th>网单金额</th>
						<th>立减优惠金额</th>
					</tr>
					</thead>
					<tbody>
					<#if ordersProducts??>
						<#list ordersProducts as ordersProduct>
							<tr>
								<td>${(ordersProduct.productName)!''}</td>
								<td>${(ordersProduct.specInfo)!''}</td>
								<td>${(ordersProduct.productSku)!''}</td>
								<td>${(ordersProduct.moneyPrice)!''}</td>
								<td>${(ordersProduct.number)!''}</td>
								<td>${(ordersProduct.moneyAmount)!''}</td>
								<td>${(ordersProduct.moneyActSingle)!''}</td>
							</tr>
						</#list>
					</#if>
					</tbody>
				</table>


				<div class="layui-form-item layui-layout-admin">
					<div class="layui-input-block">
						<div class="layui-footer" style="left: 0;">
							<#if type==0>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders" class="layui-btn">返回</a>
							<#elseif type==1>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders/state1" class="layui-btn">返回</a>
							<#elseif type==2>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders/state2" class="layui-btn">返回</a>
							<#elseif type==3>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders/state3" class="layui-btn">返回</a>
							<#elseif type==4>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders/state4" class="layui-btn">返回</a>
							<#elseif type==5>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders/state5" class="layui-btn">返回</a>
							<#elseif type==6>
								<a href="${domainUrlUtil.urlResources}/seller/order/orders/state6" class="layui-btn">返回</a>
							<#else >
								<a href="${domainUrlUtil.urlResources}/seller/order/orders" class="layui-btn">返回</a>
							</#if>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<script src="${(domainUrlUtil.staticResources)!}/resources/layui/layui.js"></script>
<script>
	layui.config({
		base: '${(domainUrlUtil.staticResources)!}/resources/'
	}).extend({
		index: 'lib/index' //主入口模块˙
	});

	layui.use(['index', 'form', 'jquery'], function(){
		var $ = layui.$
				,admin = layui.admin
				,element = layui.element
				,layer = layui.layer
				,jquery = layui.jquery
				,form = layui.form;

	});

</script>

<#include "/seller/commons/_detailfooter.ftl" />
