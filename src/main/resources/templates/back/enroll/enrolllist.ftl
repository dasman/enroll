<#include "/back/commons/_detailheader.ftl" />
<style type="text/css">
	.layui-table td .layui-table-cell{
		height:80px;
		line-height:80px;
	}
</style>
<div class="layui-fluid">
	<div class="layui-card">
		<div class="layui-form layui-card-header layuiadmin-card-header-auto" style="border-bottom: 0;">
			<div class="layui-form-item">
				<div class="layui-inline e-layui-inline">
					<label class="layui-form-label">子订单</label>
					<div class="layui-input-block">
						<input type="text" name="q_orderSn" value="${q_orderSn!''}" placeholder="请输入" autocomplete="off" class="layui-input e-layui-input">
					</div>
				</div>
				<div class="layui-inline e-layui-inline">
					<label class="layui-form-label">父订单</label>
					<div class="layui-input-block">
						<input type="text" name="q_orderPsn" value="${q_orderPsn!''}" placeholder="请输入" autocomplete="off" class="layui-input e-layui-input">
					</div>
				</div>
				<div class="layui-inline e-layui-inline">
					<label class="layui-form-label">用户名</label>
					<div class="layui-input-block">
						<input type="text" name="q_memberName" value="${q_memberName!''}" placeholder="请输入" autocomplete="off" class="layui-input e-layui-input">
					</div>
				</div>
				<div class="layui-inline e-layui-inline">
					<label class="layui-form-label">订单状态</label>
					<div class="layui-input-block">
						<@cont.select id="q_orderState" codeDiv="ORDERS_ORDER_STATE" value="${q_orderState!''}" name="q_orderState"/>
					</div>
				</div>
				<div class="layui-inline e-layui-inline">
					<label class="layui-form-label">订单类型</label>
					<div class="layui-input-block">
						<@cont.select id="q_orderType" codeDiv="ORDER_TYPE" value="${q_orderType!''}" name="q_orderType"/>
					</div>
				</div>

				<div class="layui-inline e-layui-inline">
					<label class="layui-form-label">收货电话</label>
					<div class="layui-input-block">
						<input type="text" name="q_mobile" value="${q_mobile!''}" placeholder="请输入" autocomplete="off" class="layui-input e-layui-input">
					</div>
				</div>

				<div class="layui-inline e-layui-inline">
					<button class="layui-btn layui-btn-sm" lay-submit lay-filter="search">查询</button>
				</div>
			</div>
		</div>

		<div class="layui-card-body" style="padding:0 6px 6px 6px;">
			<table class="layui-hide" id="table-data" lay-filter="table-data"></table>
			<script type="text/html" id="toolbar-button">
				<div class="layui-btn-container">
					<button class="layui-btn layui-btn-sm" lay-event="details">订单详情</button>
					<button class="layui-btn layui-btn-sm" lay-event="print">打印</button>
				</div>
			</script>
			<script type="text/html" id="toolbar-row">
				<a class="layui-btn layui-btn-xs" lay-event="row-details">详情</a>
			</script>
			<script type="text/html" id="imgTpl">
				{{#  layui.each(d.orderProductList, function(index, item){ }}
				<img height="75" src=${(domainUrlUtil.imageResources)!}/{{ item.productLeadPicpath }}>
				{{#  }); }}
				{{#  if(d.orderProductList.length === 1){ }}
				{{ d.orderProductList[0].productName }}
				{{#  } }}
			</script>
		</div>

	</div>
</div>

<script src="${(domainUrlUtil.staticResources)!}/back/layui/layui.js"></script>
<script>
	layui.config({
		base: '${(domainUrlUtil.staticResources)!}/back/'
	}).extend({
		index: 'lib/index' //主入口模块˙
	});

	layui.use(['index', 'table', 'form', 'laydate', 'jquery'], function(){
		var $ = layui.$
				,admin = layui.admin
				,table = layui.table
				,layer = layui.layer
				,laydate = layui.laydate
				,jquery = layui.jquery
				,form = layui.form;

		form.on('submit(search)', function(data){
			table.reload('table-data', {
				where: data.field
			});
		});

		table.render({
			elem: '#table-data'
			,url: '${domainUrlUtil.urlResources}/back/enroll/list/list'
			// ,height: 'full-75'
			,cellMinWidth: 80
			,page: true
			,limit: 20
			,toolbar: '#toolbar-button'
			,cols: [[
				{type:'radio'}
				,{field:'orderSn', title: '商品', width:300, templet: '#imgTpl'}
				,{field:'orderSn', title: '子订单号', width:200}
				,{field:'orderPsn', title: '父订单号', width:200}
				,{field:'memberName', title: '买家用户名', width:100}
				,{field:'orderType', title: '订单类型', width:150, templet:'<div>{{orderType(d.orderType)}}</div>'}
				,{field:'paymentStatus', title: '付款状态', width:100, templet:'<div>{{paymentStatus(d.paymentStatus)}}</div>'}
				,{field:'orderState', title: '订单状态', width:100, templet:'<div>{{getState(d.orderState)}}</div>'}
				,{field:'source', title: '订单来源', width:100, templet:'<div>{{orderSource(d.source)}}</div>'}
				,{field:'moneyProduct', title: '商品金额', width:100}
				,{field:'moneyOrder', title: '订单总金额', width:100}
				,{field:'moneyLogistics', title: '物流金额', width:100}
				,{field:'moneyCoupon', title: '优惠券金额', width:100}
				,{field:'moneyActFull', title: '订单满减金额', width:120}
				,{field:'moneyDiscount', title: '优惠总额（含单品立减）', width:200}
				,{field:'moneyBack', title: '退款金额', width:100}
				,{field:'moneyIntegral', title: '积分换算金额', width:120}
				,{field:'integral', title: '使用积分', width:100}
				,{field:'name', title: '收货人', width:100}
				,{field:'mobile', title: '收货电话', width:120}
				,{field:'addressAll', title: '省市区', width:150}
				,{field:'addressInfo', title: '详细地址', width:200}
				,{field:'invoiceStatus', title: '发票状态', width:100, templet:'<div>{{invoiceStatus(d.invoiceStatus)}}</div>'}
				,{field:'invoiceTitle', title: '发票抬头', width:180}
				,{field:'invoiceType', title: '发票类型', width:100}
				,{field:'taxNum', title: '纳税人识别号', width:200}
				,{field:'paymentName', title: '支付方式', width:150}
				,{field:'logisticsName', title: '物流名称', width:200}
				,{field:'logisticsNumber', title: '快递单号', width:200}
				,{field:'deliverTime', title: '发货时间', width:200}
				,{field:'updateTime', title: '修改时间', width:200}
				,{field:'evaluateState', title: '是否评价', width:100, templet:'<div>{{orderEvaluate(d.evaluateState)}}</div>'}
				,{fixed: 'right', title:'操作', toolbar: '#toolbar-row', width:70}
			]]
			,parseData: function(res){ //将原始数据解析成 table 组件所规定的数据
				return {
					"code": (res.success == true) ? 0 : -1, //解析接口状态
					"msg": res.message, //解析提示文本
					"count": res.total, //解析数据长度
					"data": res.rows    //解析数据列表
				};
			}
		});

		//头工具栏事件
		table.on('toolbar(table-data)', function(obj){
			var checkStatus = table.checkStatus(obj.config.id);
			switch(obj.event){
				case 'details':
					var checkData = checkStatus.data;
					if(checkData.length === 0){
						return layer.msg('请选择数据');
					}
					window.location.href="${(domainUrlUtil.urlResources)!}/seller/order/orders/details?type=0&id="+checkData[0].id;
					break;
				case 'print':
					var checkData = checkStatus.data;
					if(checkData.length === 0){
						return layer.msg('请选择数据');
					}
					window.open("${(domainUrlUtil.urlResources)!}/seller/order/orders/print?id=" + checkData[0].id, "_blank");
					break;
			};
		});

		//监听行工具事件
		table.on('tool(table-data)', function(obj){
			var data = obj.data;
			if(obj.event === 'row-details'){
				window.location.href="${(domainUrlUtil.urlResources)!}/seller/order/orders/details?type=0&id="+data.id;
			}
		});

	});

	var codeBox;


	function getState(value) {
		var box = codeBox["ORDERS_ORDER_STATE"][value];
		return box;
	}

	function paymentStatus(value) {
		var box = codeBox["ORDER_PAYMENT_STATUS"][value];
		return box;
	}

	function invoiceStatus(value) {
		var box = codeBox["ORDER_INVOICE_STATUS"][value];
		return box;
	}

	function orderType(value) {
		var box = codeBox["ORDER_TYPE"][value];
		return box;
	}

	function orderEvaluate(value) {
		var box = codeBox["ORDERS_EVALUATE"][value];
		return box;
	}

	function orderSource(value) {
		var box = codeBox["MEMBER_SOURCE"][value];
		return box;
	}

</script>

<#include "/back/commons/_detailfooter.ftl" />