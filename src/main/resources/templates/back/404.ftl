﻿<#include "/seller/commons/_detailheader.ftl" />



<div class="layui-fluid">
    <div class="layadmin-tips">
        <i class="layui-icon" face>&#xe664;</i>
        <div class="layui-text">
            <h1>
                <span class="layui-anim layui-anim-loop layui-anim-">4</span>
                <span class="layui-anim layui-anim-loop layui-anim-rotate">0</span>
                <span class="layui-anim layui-anim-loop layui-anim-">4</span>
            </h1>
        </div>
    </div>
</div>


<script src="${(domainUrlUtil.staticResources)!}/resources/layui/layui.js"></script>
<script>
    layui.config({
        base: '${(domainUrlUtil.staticResources)!}/resources/'
    }).extend({
        index: 'lib/index' //主入口模块˙
    });

    layui.use(['index'], function(){
        var $ = layui.$
            ,admin = layui.admin;

    });

</script>
<#include "/seller/commons/_detailfooter.ftl" />