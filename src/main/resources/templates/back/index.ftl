<#include "/back/commons/_detailheader.ftl" />

<body class="layui-layout-body">

<div id="LAY_app">
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header">
			<!-- 头部区域 -->
			<ul class="layui-nav layui-layout-left">
				<li class="layui-nav-item layadmin-flexible" lay-unselect>
					<a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
						<i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
					</a>
				</li>

				<li class="layui-nav-item layadmin-flexible" lay-unselect>
					<a href="javascript:;" title="交易管理">
						<i class="layui-icon" id="top-order">交易管理</i>
					</a>
				</li>



			</ul>
			<ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">

				<li class="layui-nav-item" lay-unselect>
					<a href="javascript:;" layadmin-event="refresh" title="刷新">
						<i class="layui-icon layui-icon-refresh-3"></i>
					</a>
				</li>
				<li class="layui-nav-item layui-hide-xs" lay-unselect>
					<a href="javascript:;" layadmin-event="theme">
						<i class="layui-icon layui-icon-theme"></i>
					</a>
				</li>
				<li class="layui-nav-item layui-hide-xs" lay-unselect>
					<a href="javascript:;" layadmin-event="fullscreen">
						<i class="layui-icon layui-icon-screen-full"></i>
					</a>
				</li>
				<li class="layui-nav-item" lay-unselect>
					<a href="javascript:;">
						<cite>admin</cite>
					</a>
					<dl class="layui-nav-child">
						<dd><a lay-href="">修改密码</a></dd>
						<hr>
						<dd layadmin-event="logout" style="text-align: center;"><a href="${(domainUrlUtil.urlResources)!}/back/exit">退出</a></dd>
					</dl>
				</li>

				<li class="layui-nav-item layui-hide-xs" lay-unselect>
					<a href="javascript:;" layadmin-event="about"><i class="layui-icon layui-icon-more-vertical"></i></a>
				</li>
				<li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
					<a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
				</li>
			</ul>
		</div>

		<!-- 侧边菜单 -->
		<#include "/back/commons/_left.ftl" />

		<!-- 页面标签 -->
		<div class="layadmin-pagetabs" id="LAY_app_tabs">
			<div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
			<div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
			<div class="layui-icon layadmin-tabs-control layui-icon-down">
				<ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
					<li class="layui-nav-item" lay-unselect>
						<a href="javascript:;"></a>
						<dl class="layui-nav-child layui-anim-fadein">
							<dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
							<dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
							<dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
						</dl>
					</li>
				</ul>
			</div>
			<div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
				<ul class="layui-tab-title" id="LAY_app_tabsheader">
					<li lay-id="${domainUrlUtil.urlResources}/back/enroll/list" lay-attr="${domainUrlUtil.urlResources}/back/enroll/list" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
				</ul>
			</div>
		</div>


		<!-- 主体内容 -->
		<div class="layui-body" id="LAY_app_body">
			<div class="layadmin-tabsbody-item layui-show">
				<iframe src="${domainUrlUtil.urlResources}/back/enroll/list" frameborder="0" class="layadmin-iframe"></iframe>
			</div>
		</div>

		<!-- 辅助元素，一般用于移动设备下遮罩 -->
		<div class="layadmin-body-shade" layadmin-event="shade"></div>
	</div>
</div>

<script src="${(domainUrlUtil.staticResources)!}/back/layui/layui.js"></script>
<script>
	layui.config({
		base: '${(domainUrlUtil.staticResources)!}/back/' //静态资源所在路径
	}).extend({
		index: 'lib/index' //主入口模块˙
	}).use(['index', 'form', 'jquery'], function(){
		var $ = layui.$
				,jquery = layui.jquery
				,form = layui.form
				,layer = layui.layer;

		$(document).on("click", "#top-order", function () {
			//交易管理
			$("#left-order").show();
			$("#left-orderafter").show();

			//商品管理
			$("#left-product").hide();
			$("#left-transport").hide();

			//促销活动
			$("#left-promotionbasics").hide();
			$("#left-promotion").hide();
			$("#left-operate").hide();

			//咨询评价
			$("#left-member").hide();

			//系统管理
			$("#left-pcindex").hide();
			$("#left-mobile").hide();
			$("#left-system").hide();

			//统计分析
			$("#left-settlement").hide();
		});

		$(document).on("click", "#top-product", function () {
			//交易管理
			$("#left-order").hide();
			$("#left-orderafter").hide();

			//商品管理
			$("#left-product").show();
			$("#left-transport").show();

			//促销活动
			$("#left-promotionbasics").hide();
			$("#left-promotion").hide();
			$("#left-operate").hide();

			//咨询评价
			$("#left-member").hide();

			//系统管理
			$("#left-pcindex").hide();
			$("#left-mobile").hide();
			$("#left-system").hide();

			//统计分析
			$("#left-settlement").hide();
		});

		$(document).on("click", "#top-promotion", function () {
			//交易管理
			$("#left-order").hide();
			$("#left-orderafter").hide();

			//商品管理
			$("#left-product").hide();
			$("#left-transport").hide();

			//促销活动
			$("#left-promotionbasics").show();
			$("#left-promotion").show();
			$("#left-operate").show();

			//咨询评价
			$("#left-member").hide();

			//系统管理
			$("#left-pcindex").hide();
			$("#left-mobile").hide();
			$("#left-system").hide();

			//统计分析
			$("#left-settlement").hide();
		});

		$(document).on("click", "#top-member", function () {
			//交易管理
			$("#left-order").hide();
			$("#left-orderafter").hide();

			//商品管理
			$("#left-product").hide();
			$("#left-transport").hide();

			//促销活动
			$("#left-promotionbasics").hide();
			$("#left-promotion").hide();
			$("#left-operate").hide();

			//咨询评价
			$("#left-member").show();

			//系统管理
			$("#left-pcindex").hide();
			$("#left-mobile").hide();
			$("#left-system").hide();

			//统计分析
			$("#left-settlement").hide();
		});

		$(document).on("click", "#top-system", function () {
			//交易管理
			$("#left-order").hide();
			$("#left-orderafter").hide();

			//商品管理
			$("#left-product").hide();
			$("#left-transport").hide();

			//促销活动
			$("#left-promotionbasics").hide();
			$("#left-promotion").hide();
			$("#left-operate").hide();

			//咨询评价
			$("#left-member").hide();

			//系统管理
			$("#left-pcindex").show();
			$("#left-mobile").show();
			$("#left-system").show();

			//统计分析
			$("#left-settlement").hide();
		});

		$(document).on("click", "#top-settlement", function () {
			//交易管理
			$("#left-order").hide();
			$("#left-orderafter").hide();

			//商品管理
			$("#left-product").hide();
			$("#left-transport").hide();

			//促销活动
			$("#left-promotionbasics").hide();
			$("#left-promotion").hide();
			$("#left-operate").hide();

			//咨询评价
			$("#left-member").hide();

			//系统管理
			$("#left-pcindex").hide();
			$("#left-mobile").hide();
			$("#left-system").hide();

			//统计分析
			$("#left-settlement").show();
		});



	});
</script>

<#include "/back/commons/_detailfooter.ftl" />