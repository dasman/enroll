<#import "/back/commons/_macro_controller.ftl" as cont/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="${(domainUrlUtil.staticResources)!}/back/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${(domainUrlUtil.staticResources)!}/back/style/admin.css" media="all">
</head>
