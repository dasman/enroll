<div class="layui-side layui-side-menu">
    <div class="layui-side-scroll">
        <div class="layui-logo" lay-href="back/enroll/list">
            <span>后台</span>
        </div>

        <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
            <li data-name="home" class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;" lay-tips="主页" lay-direction="2">
                    <i class="layui-icon layui-icon-home"></i>
                    <cite>主页</cite>
                </a>
                <dl class="layui-nav-child">
                    <dd data-name="console" class="layui-this">
                        <a lay-href="${domainUrlUtil.urlResources}/back/enroll/list">登记</a>
<!--                        <a lay-href="${domainUrlUtil.urlResources}/back/console">控制台</a>-->
                    </dd>
                </dl>
            </li>

            <!--交易管理-->
                <li data-name="template" class="layui-nav-item" id="left-order">
                    <a href="javascript:;" lay-tips="交易管理" lay-direction="2">
                        <i class="layui-icon layui-icon-template"></i>
                        <cite>交易管理</cite>
                    </a>
                    <dl class="layui-nav-child">
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders">全部订单</a></dd>
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders/state1">未付款订单</a></dd>
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders/state2">待确认订单</a></dd>
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders/state3">待发货订单</a></dd>
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders/state4">已发货订单</a></dd>
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders/state5">已完成订单</a></dd>
                            <dd><a lay-href="${domainUrlUtil.urlResources}/seller/order/orders/state6">已取消订单</a></dd>
                    </dl>
                </li>





        </ul>
    </div>
</div>