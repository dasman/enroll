package com.sapuo.enroll.constant;

public class MobileCheckConstant {
	public final static String CACHE_PREFIX ="MobileCheck:";
	public final static String MOBILES_CACHE_KEY =String.format("%smobiles_list", CACHE_PREFIX);
	/**
	 * 通用黑/白名单缓存key
	 */
	public final static String COMMON_CACHE_KEY= CACHE_PREFIX.concat("common_zq_check_cache:");
	/**
	 * 通用错误编码
	 */
	public final static int ERROR_CODE_COMMON =1;

	public static final String JASYPT_SALT = "zqmall-1qaz2wsx!QAZ@WSX";

}
