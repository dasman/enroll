package com.sapuo.enroll.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;


/**
 * 日期工具类
 * @author xinqiangyi
 *
 * 2017年12月20日
 */
public class DateUtil {
	
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    
    public static final String YYYYMMDD = "yyyyMMdd";

    public static final String YYYYMM = "yyyyMM";
    
    /** 时间格式化pattern，日期横杠拆分，时间冒号拆分 */
    public static final String DAY_CROSS_TIME_COLON = "yyyy-MM-dd HH:mm:ss";
	
	/** 
	 * 返回当前系统日期
	 * @return
	 */
	public static String getSysCurrDate(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return formatter.format(new Date());

	}
	
	/**
	 * 当前月份
	 * @return
	 */
	public static int getSysMonth(){
		Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH) + 1;//月
        return month;
	}

	/**
	 * 当前日期
	 * @return
	 */
	public static String getSystemDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String date = formatter.format(new Date());
		return date;
	}

	/**
	 * 当月第二天（每月二号）
	 * @return
	 */
	public static String getSecondDay() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar1=Calendar.getInstance();
		calendar1.set(Calendar.DAY_OF_MONTH,2);
		return sdf.format(calendar1.getTime());
	}

	/**
	 * 返回当前系统时间--时
	 * @return
	 */
	public static String getSysCurrTime(){
		SimpleDateFormat formatter = new SimpleDateFormat("HH");
		return formatter.format(new Date());

	}

	/***
	 * 获取年月日 时分秒毫秒
	 * yyyy-MM-dd HH:mm:ss:SSS
	 * @return
	 */
	public static String getNewDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		String date = formatter.format(new Date());
		return date;
	}

	public static String getSmsSeed() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String date = formatter.format(new Date());
		return date;
	}

	public static String getYm(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMM");
		return formatter.format(new Date());

	}

	public static String getYMD() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		return formatter.format(new Date());
	}

	public static String getYmd() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(new Date());

	}
	public static String formatYMDHMSM(Date date) {
		return new SimpleDateFormat("yyyyMMDDHHmmssSSS").format(date);
	}

	public static String getYesterday() throws ParseException {
		SimpleDateFormat sj = new SimpleDateFormat("yyyy-MM-dd");
		Date d = new SimpleDateFormat("yyyy-MM-dd").parse(DateUtil.getYmd());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.add(Calendar.DATE, -1);
		String yesterday = sj.format(calendar.getTime());// 昨天
		return yesterday;
	}
	/**
	 * 判断日期是否超过90天
	 */
	public static boolean isOutOfDate(String loginTime) throws Exception {
		long margin = 0;
		Date nowDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// 现在时间
		nowDate = sdf.parse(sdf.format(nowDate));
		// 状态更新时间
		Date UpdateTime = sdf.parse(loginTime);
		margin = nowDate.getTime() - UpdateTime.getTime();
		margin = margin / (1000 * 60 * 60 * 24);
		if (margin > 90) {
			return true;
		}
		return false;
	}



	/**
	 * 返回近几天
	 */
	public static List<String> getSeveralDays(int past) {
		ArrayList<String> pastDaysList = new ArrayList<>();
		for (int i = past; i > 0; i--) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - i);
			Date today = calendar.getTime();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String result = format.format(today);
			pastDaysList.add(result);
		}
		return pastDaysList;
	}

	public static int compare_date(String DATE1, String DATE2, DateFormat df) {
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			if (dt1.getTime() > dt2.getTime()) {
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				return -1;
			} else {
				return 0;
			}
		} catch (Exception exception) {

		}
		return 0;
	}

	public static int compare_date_three(String DATE1, String DATE2, String DATE3, String type, DateFormat df) {
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			Date dt3 = df.parse(DATE3);
			if (type.equals("1")) {
				if (dt2.getTime() >= dt1.getTime()) {
					if (dt2.getTime() <= dt3.getTime()) {
						return 1;
					}
				}
			} else if (type.equals("2")) {
				if (dt2.getTime() > dt1.getTime()) {
					if (dt2.getTime() < dt3.getTime()) {
						return 1;
					}
				}
			}

		} catch (Exception exception) {
	
		}
		return 0;
	}

	/**
	 * 格式化相对时间【1小时以内（不含1小时），显示**分钟前，24小时以内（不含24小时），显示**小时前，1天以上（含1天），显示YYYY-MM-
	 * DD；】
	 * 
	 * @param dateStr
	 *            时间字符串
	 * @return 格式化后的时间字符串
	 */
	public static String formatReleaseDate(String dateStr) {
		if (StringUtils.isEmpty(dateStr)) {
			return "";
		}
		if (dateStr.contains(".")) {
			dateStr = dateStr.substring(0, dateStr.indexOf("."));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date;
		try {
			date = sdf.parse(dateStr);
			if (date == null) {
				return dateStr;
			}
			long delta = new Date().getTime() - date.getTime();
			if (delta < 60L * 60000L) {
				long minutes = (delta / 1000L) / 60L;
				return (minutes <= 0 ? 1 : minutes) + "分钟前";
			}
			if (delta < 24L * 3600000L) {
				long hours = ((delta / 1000L) / 60L) / 60L;
				return (hours <= 0 ? 1 : hours) + "小时前";
			}
			SimpleDateFormat sdfRes = new SimpleDateFormat("yyyy-MM-dd");
			return sdfRes.format(date);
		} catch (ParseException e) {
			return dateStr;
		}
		
	}
	
	/**
	 * 
	 * 传入日期跟 系统日志之间的天数
	 * 
	 * @param endDate
	 * @return
	 */
	  public static long disDay(String endDate) throws ParseException {
	        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        String date = df.format(new Date());
	        Date startTime = df.parse(date);
	        Date endTime = df.parse(endDate);
	        long time = startTime.getTime() - endTime.getTime();
	        long day = time / (3600 * 24 * 1000);
	        return Math.abs(day);
	    }
	  
	  
	  public static long disSecond(String starDate,String endDate) throws ParseException {
		  long second =0;
	        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        Date startTime = df.parse(starDate);
	        Date endTime = df.parse(endDate);
	        long time = endTime.getTime() - startTime.getTime();
	        second = time / 1000;
	        if(second<0){
	        	second =0;
	        }
	        return second;
	    }
	  
	  public static int weeks(String endDate) throws ParseException {
	        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	        String date = df.format(new Date());
	        Date startTime = df.parse(date);
	        Date endTime = df.parse(endDate);
	        long time = startTime.getTime() - endTime.getTime();
	        long day = time / (3600 * 24 * 1000);
	        int days =(int)day +1;
	        int a =  days/7;
	        int b =  days%7;
	        int week =a;
	        if(b>0){
	        	week = a +1;
	        }
	        return week;
	    }

	/**
	 *
	 * @param nowTime   当前时间
	 * @param startTime	开始时间
	 * @param endTime   结束时间
	 * @return
	 * @author sunran   判断当前时间在时间区间内
	 */
	public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
		if (nowTime.getTime() == startTime.getTime()
				|| nowTime.getTime() == endTime.getTime()) {
			return true;
		}

		Calendar date = Calendar.getInstance();
		date.setTime(nowTime);

		Calendar begin = Calendar.getInstance();
		begin.setTime(startTime);

		Calendar end = Calendar.getInstance();
		end.setTime(endTime);

		if (date.after(begin) && date.before(end)) {
			return true;
		} else {
			return false;
		}
	}
	  
	  
	  
    public static String formatDateByDTF(Date date, String pattern)
    {

        DateTimeFormatter df = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime ldt = date.toInstant()
                                .atZone(ZoneId.systemDefault())
                                .toLocalDateTime();
        return df.format(ldt);
    }
}
