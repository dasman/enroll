package com.sapuo.enroll.utils;

import org.apache.commons.lang3.StringUtils;

public class FileNameUtils {

	public static String getLegalName(String fileName) {
		String strInjectListStr = "../|./|/..| |<|>|:|?";
		if (StringUtils.isNotBlank(fileName)) {
			fileName = fileName.toLowerCase();
			String[] badStrs = strInjectListStr.split("\\|");
			for (int i = 0; i < badStrs.length; i++) {
				if (fileName.indexOf(badStrs[i]) >= 0) {
					fileName = fileName.replace(badStrs[i], "");
				}
			}
		}
		return fileName;
	}

}
