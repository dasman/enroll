package com.sapuo.enroll.message.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author sch
 * @date 2021/11/18
 * @apiNote
 */
@Data
@ApiModel
public class AddValuesReq {
    @ApiModelProperty(notes = "号码串", example = "13800138000,13800138001")
    private String values;
    @ApiModelProperty(notes = "清单标识",example = "11")
    private String key;

    @ApiModelProperty(notes = "清单标识",example = "11")
    private String type;
}
