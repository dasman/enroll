package com.sapuo.enroll.message.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel
@Data
@Validated
public class EditReq {

    @ApiModelProperty(notes = "白名单关键字",example = "11")
    @NotNull @NotBlank
    String key;
    @NotNull
    String value;
    @NotNull
    String editValue;

}
