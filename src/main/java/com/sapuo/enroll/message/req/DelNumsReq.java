package com.sapuo.enroll.message.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class DelNumsReq {
	@ApiModelProperty(notes = "号码串", example = "13800138000,13800138001")
	private String nums;
}
