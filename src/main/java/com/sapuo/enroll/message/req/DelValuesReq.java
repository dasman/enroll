package com.sapuo.enroll.message.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class DelValuesReq {
    @ApiModelProperty(notes = "values", example = "13800138000,13800138001")
    private String values;
    @ApiModelProperty(notes = "白名单标识", example = "11")
    private String key;
}
