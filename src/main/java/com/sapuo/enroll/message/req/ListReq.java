package com.sapuo.enroll.message.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class ListReq {
    @ApiModelProperty(notes = "页数", example = "1")
    private int page;
    @ApiModelProperty(notes = "每页条数", example = "2")
    private int rows;

    @ApiModelProperty(notes = "清单标识",example = "11")
    private String key;

    @ApiModelProperty(notes = "清单标识",example = "11")
    private String type;

    @ApiModelProperty(notes = "搜索手机号",example = "11")
    private String phone;


}
