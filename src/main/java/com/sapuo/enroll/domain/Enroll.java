package com.sapuo.enroll.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Enroll implements Serializable {

    private Long id;

    private String name;
    private String password;

    private String appToken;

    private String appTokenStatus;

    private String applyStatus;

    private String enrollStatus;

    private String phone;

    private String reCode;

    private Date doTime;

    private Date closeTime;

    private Integer isEnter;

    private Integer isAuto;

    private Date createTime;
    private Date updateTime;



}
