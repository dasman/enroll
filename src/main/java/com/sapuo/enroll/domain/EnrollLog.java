package com.sapuo.enroll.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnrollLog implements Serializable {


    private Long id;

    private Long deviceId;

    private Integer status;

    private String msg_code;

    private Date createTime;

    private Date endTime;



}

