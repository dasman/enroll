package com.sapuo.enroll.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EnrollDevice implements Serializable {
    private Long id;

    private String fingerprint;

    private String imei;

    private String mac;

    private Long cdkeyId;

    private String cdkey;

    private String ip;

    private String token;

    private Date createTime;

    private Date updateTime;


}
