package com.sapuo.enroll.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CDKey implements Serializable {

    private Long id;

    private String cdkey;

    private String belong;

    private Integer isUsed;

    private Date createTime;

    private Date useTime;


    private Date updateTime;


}
