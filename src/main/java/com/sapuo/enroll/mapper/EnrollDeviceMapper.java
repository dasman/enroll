package com.sapuo.enroll.mapper;

import com.sapuo.enroll.domain.CDKey;
import com.sapuo.enroll.domain.Enroll;
import com.sapuo.enroll.domain.EnrollDevice;
import org.mapstruct.Mapper;

@Mapper
public interface EnrollDeviceMapper {

    EnrollDevice get(Integer id);


    Integer insert(EnrollDevice device);

    Integer update(EnrollDevice device);

    EnrollDevice getByCdkey(String cdkey);


}
