package com.sapuo.enroll.mapper;

import com.sapuo.enroll.domain.Enroll;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface EnrollMapper {

    Enroll get(Integer id);


    Integer insert(Enroll enroll);

    Integer update(Enroll enroll);
    Integer updateToken(Enroll enroll);

    List<Enroll> getActBiddingState5(@Param("actBiddingId") Integer actBiddingId);

}
