package com.sapuo.enroll.mapper;

import com.sapuo.enroll.domain.CDKey;
import com.sapuo.enroll.domain.Enroll;
import com.sapuo.enroll.domain.EnrollDevice;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CDKeyMapper {

    CDKey get(Integer id);


    Integer insert(CDKey cdkey);

    Integer update(CDKey cdkey);


    CDKey getByCdkey(String cdkey);

}
