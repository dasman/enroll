package com.sapuo.enroll.dto;

import lombok.Data;

import java.util.Date;


@Data
public class EnrollReq {

    private String name;
    private String password;

    private String appToken;

    private String appTokenStatus;

    private String applyStatus;

    private String enrollStatus;

    private String phone;

    private String reCode;

    private Date doTime;

    private Date closeTime;

    private Integer isEnter;

    private Integer isAuto;
}
