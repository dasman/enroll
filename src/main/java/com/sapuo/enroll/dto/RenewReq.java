package com.sapuo.enroll.dto;

import lombok.Data;

@Data
public class RenewReq {


    private String cdkey;
    private String sign;
    private String token;
}
