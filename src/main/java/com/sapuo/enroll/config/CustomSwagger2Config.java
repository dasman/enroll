//
//package com.sapuo.enroll.config;
//
//import static com.google.common.base.Predicates.and;
//import static com.google.common.base.Predicates.containsPattern;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.google.common.base.Predicate;
//
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
///*
// * Restful API 访问路径:
// * http://IP:port/{context-path}/swagger-ui.html
// * eg:http://localhost:8080/swagger-ui.html
// */
//@EnableSwagger2
//@Configuration
//public class CustomSwagger2Config
//{
//
//    @Bean
//    public Docket cellPhone_api() {
//        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.ant("/mobile/**")).build().groupName("号码校验接口V1.0").pathMapping("/")
//                .apiInfo(apiInfo("号码白名单管理、校验接口","文档中可以查询及测试接口调用参数和结果","1.0"));
//    }
//    @Bean
//    public Docket common_api() {
//        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.ant("/common/**")).build().groupName("通用白名单管理接口V1.0").pathMapping("/")
//                .apiInfo(apiInfo("白名单管理、校验接口","文档中可以查询及测试接口调用参数和结果","1.0"));
//    }
//
//
//    private ApiInfo apiInfo(String name,String description,String version) {
//        ApiInfo apiInfo = new ApiInfoBuilder().title(name).description(description).version(version).build();
//        return apiInfo;
//    }
//
//    private ApiInfo apiInfo()
//    {
//
//        return new ApiInfoBuilder().title("接口API")
//                                   .description("API接口规则：必须要以/api开头")
//                                   .termsOfServiceUrl("http://www.baidu.com/")
//                                   .version("1.0")
//                                   .build();
//    }
//
//    @SuppressWarnings({ "unchecked", "unused" })
//    private Predicate<String> paths()
//    {
//
//        return and(containsPattern(".*api.*"));
//    }
//}
