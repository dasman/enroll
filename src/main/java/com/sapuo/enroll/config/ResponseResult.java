package com.sapuo.enroll.config;

import com.sapuo.enroll.constant.MobileCheckConstant;

public class ResponseResult {
	static String SUCCESS = "ok";
	static String ERROR = "error";
	static int UNKOWN_ERROR=999;

	public static <T> Result<T> makeOKRsp(T data) {
		if (null == data) {
			return makeOKRsp(200, SUCCESS);
		}
		return new Result<T>(200, SUCCESS, data);
	}

	public static Result makeCommonError(String msg){
		return new Result(MobileCheckConstant.ERROR_CODE_COMMON,msg);
	}

	public static <T> Result<T> makeListRsp(T data,String records){
		return new Result<T>(200, SUCCESS, data,records);
	}
/**
 * 
 * @param <T>
 * @param status 结果码
 * @param msg 消息
 * @return
 */
	public static <T> Result<T> makeOKRsp(Integer status, String msg) {
		return new Result<T>(status, msg, null);
	}
	
	public static <T> Result<T> makeUnkownError(T data) {
		if (null == data) {
			return makeOKRsp(UNKOWN_ERROR, "not catched.");
		}
		return new Result<T>(UNKOWN_ERROR, "not catched.", data);
	}
	/**
	 * 
	 * @param <T>
	 * @param status 结果码
	 * @param msg 消息
	 * @param additions 没有时可以传null
	 * @return
	 */
	public static <T> Result<T> makeError(Integer status, String msg, Object[] additions) {
		Result<T> result = makeOKRsp(status,msg);

		result.setAdditions(additions);
		return result;
	}
}
