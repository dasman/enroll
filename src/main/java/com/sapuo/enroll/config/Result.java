package com.sapuo.enroll.config;

import lombok.Data;

@Data
public class Result<T> {
	private Integer code;
	private String message;
	private String msg;
	private Object[] additions;
	private String records;

	private T data;

	public Result(T data) {
		super();
		this.data = data;
	}

	public Result(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
		this.msg = message;

	}

	public Result(Integer code, String message, T data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;

		this.msg = message;

	}

	public Result(Integer code, String message, T data , String records) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
		this.records = records;

		this.msg = message;


	}

}
