package com.sapuo.enroll.config;


import org.apache.shiro.realm.Realm;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Bean
    public Realm realm(){
        SimpleAccountRealm realm = new SimpleAccountRealm();
        realm.addAccount("admin","111111","admin");
        return realm;
    }

    @Bean
    public DefaultWebSecurityManager SecrityManager(){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(this.realm());
        return securityManager;

    }

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(){
        ShiroFilterFactoryBean filterFactoryBean = new ShiroFilterFactoryBean();

        filterFactoryBean.setSecurityManager(this.SecrityManager());
        filterFactoryBean.setLoginUrl("/back/login"); //登陆url
        filterFactoryBean.setSuccessUrl("/back/index");//成功url
        filterFactoryBean.setUnauthorizedUrl("/back/unauth");  //无权限url

        filterFactoryBean.setFilterChainDefinitionMap(this.filterChainDefinitionMap());
        return filterFactoryBean;
    }

    private Map<String,String> filterChainDefinitionMap(){
        Map<String,String> filterMap = new LinkedHashMap<>();
        filterMap.put("/resources/**","anon");//可以匿名访问
        filterMap.put("/system/verifyCode","anon");//可以匿名访问
        filterMap.put("/back/doLogin","anon");//可以匿名访问
        filterMap.put("/back/unauth","anon");//可以匿名访问


        filterMap.put("/back/index","anon");//
        filterMap.put("/back/enroll/list","anon");//


        filterMap.put("/back/exit","logout");//退出登陆

//        filterMap.put("/**","authc"); //

        return filterMap;

    }

}
