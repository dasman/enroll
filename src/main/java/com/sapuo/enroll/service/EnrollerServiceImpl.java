package com.sapuo.enroll.service;

import com.sapuo.enroll.domain.Enroll;
import com.sapuo.enroll.mapper.EnrollMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EnrollerServiceImpl implements EnrollService{

    @Autowired
    EnrollMapper enrollMapper;

    @Override
    public Enroll getById(Long id) {
        return null;
    }

    @Override
    public Integer insert(Enroll enroll) {

        enrollMapper.insert(enroll);

        return null;
    }

    @Override
    public Integer update(Enroll enroll) {
        return null;
    }

    @Override
    public Integer detele() {
        return null;
    }
}
