package com.sapuo.enroll.service;

import com.sapuo.enroll.domain.Enroll;

public interface EnrollService {

    Enroll getById(Long id);

    Integer insert(Enroll enroll);

    Integer update(Enroll enroll);

    Integer detele();
}
