package com.sapuo.enroll.controller;

import com.sapuo.enroll.domain.CDKey;
import com.sapuo.enroll.domain.Enroll;
import com.sapuo.enroll.domain.EnrollDevice;
import com.sapuo.enroll.mapper.CDKeyMapper;
import com.sapuo.enroll.mapper.EnrollDeviceMapper;
import com.sapuo.enroll.utils.CDKeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@Slf4j
public class CDKeyController {

    @Autowired
    CDKeyMapper cdKeyMapper;

    @Autowired
    EnrollDeviceMapper enrollDeviceMapper;

//    @RequestMapping(value = "cdkey/add", method = { RequestMethod.POST })
//    @ResponseBody
//    public String cdkeyAdd(HttpServletRequest request, HttpServletResponse response,
//                           CDKey cDKey) throws Exception {
//        String CDKey = CDKeyUtil.createCDkey("02", "3", "1");
//
//        CDKey cdKey = new CDKey();
//        cdKey.setCdkey(CDKey);
//        cdKey.setBelong("2"); //1，7天， 2，三个月   ，3，永久
//        cdKey.setIsUsed(0);
//        cdKey.setCreateTime(new Date());
//        Integer a =  cdKeyMapper.insert(cdKey);
//
////        for (int i = 0; i < 10; i++) {
////            String CDKey = CDKeyUtil.createCDkey("67", "0", "1");
////            System.out.println("激活码：" + CDKey);
////            String deCDkey = CDKeyUtil.deCDkey(CDKey, "1");
////            System.out.println("激活码解密：" + deCDkey);
////        }
//
//        System.out.println(a);
//
//        return "enroll";
//    }

}

