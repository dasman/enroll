//package com.sapuo.enroll.controller;
//
//
//import com.sapuo.enroll.domain.Enroll;
//import com.sapuo.enroll.service.EnrollerServiceImpl;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Map;
//
//@Controller
//@Slf4j
//public class EnrollController {
//
//    @Autowired
//    EnrollerServiceImpl enrollerService;
//
//    //第一步
//    @RequestMapping(value = "enroll/add", method = { RequestMethod.POST })
//    @ResponseBody
//    public String enrollAdd(HttpServletRequest request, HttpServletResponse response,
//                              Enroll enroll) {
//
//        enrollerService.insert(enroll);
//
//        return "enroll";
//    }
//
//
//    @RequestMapping(value = "enroll/index", method = { RequestMethod.GET })
//    public String enrollFirst(HttpServletRequest request, HttpServletResponse response,
//                           Map<String, Object> dataMap) {
//
//
//        return "enroll";
//    }
//
//    @RequestMapping(value = "enroll/pay", method = { RequestMethod.GET })
//    public String pay(HttpServletRequest request, HttpServletResponse response,
//                           Map<String, Object> dataMap) {
//
//
//        return "pay";
//    }
//
//    @RequestMapping(value = "enroll/login", method = { RequestMethod.GET })
//    public String login(HttpServletRequest request, HttpServletResponse response,
//                           Map<String, Object> dataMap) {
//
//
//        return "login";
//    }
//
//
//    //我的
//    @RequestMapping(value = "enroll/mine", method = { RequestMethod.GET })
//    public String mine(HttpServletRequest request, HttpServletResponse response,
//                        Map<String, Object> dataMap) {
//
//
//        return "mine";
//    }
//
//
//
//}
