//package com.sapuo.enroll.controller.back;
//
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Map;
//
//@Controller
//@RequestMapping(value = "back/enroll")
//public class BackEnrollController {
//
//    @RequestMapping(value = "list", method = { RequestMethod.GET })
//    public String list(HttpServletRequest request, HttpServletResponse response,
//                         Map<String, Object> dataMap) {
//
//
//        return "back/enroll/enrolllist";
//    }
//
//    @RequestMapping(value = "listjson", method = { RequestMethod.GET })
//    @ResponseBody
//    public String listjson(HttpServletRequest request, HttpServletResponse response,
//                         Map<String, Object> dataMap) {
//
//
//
//        return "back/enroll/enrolllist";
//    }
//}
