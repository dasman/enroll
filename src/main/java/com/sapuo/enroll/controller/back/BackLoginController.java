//package com.sapuo.enroll.controller.back;
//
//
//import com.sapuo.enroll.config.Result;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.shiro.SecurityUtils;
//import org.apache.shiro.authc.DisabledAccountException;
//import org.apache.shiro.authc.IncorrectCredentialsException;
//import org.apache.shiro.authc.UnknownAccountException;
//import org.apache.shiro.authc.UsernamePasswordToken;
//import org.apache.shiro.subject.Subject;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.DigestUtils;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.util.Date;
//import java.util.Enumeration;
//import java.util.Map;
//
//@Controller
//@RequestMapping(value = "back")
//@Slf4j
//public class BackLoginController {
//
//    @RequestMapping(value = "login", method = { RequestMethod.GET })
//    public String login(HttpServletRequest request, HttpServletResponse response,
//                        Map<String, Object> dataMap) {
//
//        return "back/login";
//    }
//
//    @RequestMapping(value = "/doLogin", method = { RequestMethod.POST })
//    public @ResponseBody Result<Boolean> doLogin(Map<String, Object> dataMap, HttpServletRequest request,
//                                                 HttpServletResponse response) throws Exception {
//        String name = request.getParameter("name");
//        String password = request.getParameter("password");
//        String verifyCode = request.getParameter("verifyCode");
//        String verify_number = (String) request.getSession()
//                .getAttribute("verify_number");
//
//        if (name == null) {
//            return new Result<>(1,"用户名不能为空");
//        }
//        if (password == null) {
//            return new Result<>(1,"密码不能为空");
//        }
//
//        if (verify_number == null || !verify_number.equalsIgnoreCase(verifyCode)) {
//            return new Result<>(1,"验证码不正确");
//        }
//
//        Subject subject = SecurityUtils.getSubject();
//
//        //spring md5加密
//        String md5Str = DigestUtils.md5DigestAsHex(password.getBytes());
//        UsernamePasswordToken token = new UsernamePasswordToken(name,
//                password);
//        // token.setRememberMe(true);
//        try {
//            subject.login(token);
//
//
////            WebSellerSession.putSellerUser(request, sellerUser);
//
//        }  catch (Exception e) {
//            log.error(e.getMessage());
//            // 其他异常时退出
//            subject.logout();
//        }
//
//        return new Result<>(0,"登陆成功");
//    }
//
//    @RequestMapping(value = "unauth", method = { RequestMethod.GET })
//    public String unauth(HttpServletRequest request, HttpServletResponse response,
//                        Map<String, Object> dataMap) {
//
//        return "back/unauth";
//    }
//
//    @RequestMapping(value = "/exit", method = { RequestMethod.GET })
//    public String exit(HttpServletRequest request) throws Exception {
//
//        Subject subject = SecurityUtils.getSubject();
//        subject.logout();
//
//        HttpSession session = request.getSession();
//        Enumeration<?> em = session.getAttributeNames();
//        //清空session
//        while (em.hasMoreElements()) {
//            session.removeAttribute(em.nextElement().toString());
//        }
//        //清除cookie
//        Cookie[] cookies = request.getCookies();
//        for (Cookie cookie : cookies) {
//            cookie.setMaxAge(0);
//        }
//        //重定向
//        return "redirect:/back/login";
//    }
//}
